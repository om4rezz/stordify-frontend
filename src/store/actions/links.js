import axios from "axios";
import {SHORTEN_URL} from "./actionTypes";
import {SHORTEN_URL_ENDPOINT} from "../../helpers/endpoints";
import {HOST} from "../../helpers/constants";
import {openNotificationWithIcon} from '../../helpers/helper_functions';

export const shortenURL = (url) => ({
    type: SHORTEN_URL,
    payload: url,
});

export const generateShortenedURL = (original_url) => {
    const URL = `${HOST + SHORTEN_URL_ENDPOINT}`;

    return dispatch => {
        axios.post(URL, {
            "link": {
                "original_url": original_url
            }
        }, {
            headers: {
                Accept: '*/*',
            },
        }).then(res => {
            openNotificationWithIcon('success', 'Succeeded', 'Link successfully shortened!');
            dispatch(shortenURL(res.data));
        }).catch(err => {
            openNotificationWithIcon('error', 'Invalid URL', 'The URL you entered is bad-formatted!')
        });
    }
}