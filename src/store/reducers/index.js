import {combineReducers} from 'redux';
import linksReducer from './links';

export default combineReducers({
    linksReducer,
});
