import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';
import {HOST} from "../../helpers/constants";

const initialState = {
  shortened_url: "",
};

const linksReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SHORTEN_URL:
      return updateObject(state, {
        shortened_url: `${HOST}/${action.payload.slug}`,
      });

    default:
      return state;
  }
};

export default linksReducer;
