import React from 'react';
import RouteWithLayout from './helpers/RouteWithLayout';
import MainLayout from './containers/MainLayout';
import Shortener from './components/link_shortener/Shortener';

const BaseRouter = (props) => (
  <div>
    <RouteWithLayout exact path='/' component={Shortener} layout={MainLayout} />
  </div>
);

export default BaseRouter;
