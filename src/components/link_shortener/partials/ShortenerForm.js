import React, {useEffect, useState} from 'react';
import {
    Form,
    Input,
    Button, Alert,
} from 'antd';
import {ThunderboltOutlined} from "@ant-design/icons";
import {useDispatch, useSelector} from 'react-redux';
import {generateShortenedURL} from "../../../store/actions/links";

const ShortenerForm = () => {
    const [originalURL, setOriginalURL] = useState('');
    const [isShownShortenedURL, setShowShortenedURL] = useState(false);

    const shortenedURL = useSelector(state => state.linksReducer.shortened_url);
    const dispatch = useDispatch();

    useEffect(() => {
        if (shortenedURL.length > 0) {
            setShowShortenedURL(true);
        }else{
            setShowShortenedURL(false);
        }
    }, [shortenedURL]);

    const onFinish = (values) => {
        console.log('Success:', values);
        dispatch(generateShortenedURL(originalURL));
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    return (
        <Form
            labelCol={{
                span: 4,
            }}
            wrapperCol={{
                span: 14,
            }}
            layout="horizontal"
            initialValues={{
                size: "large",
                remember: true,
            }}
            size={"large"}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
        >
            <Form.Item
                label="Original URL"
                name="original_url"
                rules={[
                    {
                        required: true,
                        message: 'Please provide a valid URL!',
                    },
                ]}
            >
                <Input onChange={(e) => setOriginalURL(e.target.value)}/>
            </Form.Item>
            <Form.Item label=" ">
                <Button type="primary" htmlType="submit" shape="round" icon={<ThunderboltOutlined/>} size={"large"}>
                    Shorten URL
                </Button>
                <br/>
                <br/>

                {isShownShortenedURL ?
                    <Alert
                        message="Shortened URL:"
                        description={<a target="_blank" href={shortenedURL}>{shortenedURL}</a>} type="success"
                        type="success"
                        showIcon
                    />
                    :
                    null
                }
            </Form.Item>
        </Form>
    );
};

export default ShortenerForm;