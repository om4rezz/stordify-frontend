import React from "react";
import { LinkOutlined } from '@ant-design/icons';
import ShortenerForm from "./partials/ShortenerForm";
const Shortener = () => {
    return (
        <>
            <h1>Hello Stordify</h1>
            <h3>Shorten your URLs <LinkOutlined /> like a boss!</h3>
            <ShortenerForm/>
        </>
    )
}

export default Shortener;